import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;

import org.junit.Test;


public class Lab1Tests {

	/**
	 * Test esistenza classe linked list:
	 * Deve esistere una classe chiamata LinkedList.
	 */
	@Test
	public void test1() {
		try{
			Class.forName("LinkedList");
		}catch(ClassNotFoundException e){
			fail("LinkedList not yet implemented");
		}
	}
	
	/**
	 * Test istanziazione linked list vuota:
	 * Il costruttore di default deve inizializzare una lista vuota.
	 * La descrizione della lista vuota deve essere "[]".
	 */
	@Test
	public void test2() {
		LinkedList l = new LinkedList();
		assertEquals("[]", l.toString());
	}
	
	/**
	 * Test esistenza costruttore linked list da stringa:
	 * Deve esistere un costruttore che prende come parametro un oggetto di tipo String.
	 */
	@Test
	public void test3() {
		try{
			LinkedList.class.getConstructor(java.lang.String.class);
		}catch(NoSuchMethodException e){
			fail("Contructor not yet implemented");
		}
	}
	
	/**
	 * Test istanziazione linked list da stringa:
	 * Il costruttore da String inizializza una lista con gli elementi specificati.
	 * Ogni elemento (numero reale) e' separato da uno spazio.
	 */
	@Test
	public void test4() {
		LinkedList l = new LinkedList("1 2 3");
		assertEquals("[1.0 2.0 3.0]", l.toString());
		l = new LinkedList("1.23 2.001 3.75");
		assertEquals("[1.23 2.001 3.75]", l.toString());
		l = new LinkedList("");
		assertEquals("[]", l.toString());
		
	}
	
	/**
	 * Test istanziazione linked list da stringa malformata:
	 * Se la stringa fornita in argomento al costruttore non e' formattata correttamente,
	 * deve essere sollevata un'eccezione di tipo IllegalArgumentException.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void test4b() {
		new LinkedList("1 2 abb");
	}
	
	/**
	 * Test metodi add:
	 * Devono esistere due metodi addFirst e addLast che accettano entrambi un double come parametro.
	 * addFirst inserisce un nuovo elemento nella prima posizione della lista.
	 * addLast inserisce un nuovo elemento nell'ultima posizione della lista.
	 */
	@Test
	public void test5() {
		try{
			LinkedList.class.getMethod("addFirst", double.class);
			LinkedList.class.getMethod("addLast", double.class);
		}catch(NoSuchMethodException e){
			fail("Method not yet implemented");
		}
		LinkedList l = new LinkedList();
		l.addFirst(1);
		assertEquals("[1.0]", l.toString());
		l.addFirst(3.75);
		assertEquals("[3.75 1.0]", l.toString());
		l.addLast(1.25);
		assertEquals("[3.75 1.0 1.25]", l.toString());
		l = new LinkedList();
		l.addFirst(1);
		LinkedList l2 = new LinkedList();
		l2.addLast(1);
		assertEquals(l, l2);
		
	}
	
	/**
	 * Test metodi remove:
	 * Devono esistere i metodi remove che accetta un double come parametro
	 * e i metodi removeFirst, removeLast (senza parametri).
	 * Il metodo remove, rimuove la prima occorrenza dell'elemento specificato. Ritorna true se
	 * l'elemento e' stato trovato nella lista, false altrimenti.
	 * I metodi removeFirst e removeLast rimuovono rispettivamente il primo e l'ultimo elemento.
	 * Ritornano true se rimuovono l'elemento, false altrimenti.
	 */
	@Test
	public void test6() {
		try{
			LinkedList.class.getMethod("remove", double.class);
			LinkedList.class.getMethod("removeFirst");
			LinkedList.class.getMethod("removeLast");
		}catch(NoSuchMethodException e){
			fail("Method not yet implemented");
		}
		LinkedList l = new LinkedList("1 2 3 3 4 5 4");
		assertEquals("[1.0 2.0 3.0 3.0 4.0 5.0 4.0]", l.toString());
		assertTrue(l.remove(2));
		assertEquals("[1.0 3.0 3.0 4.0 5.0 4.0]", l.toString());
		assertTrue(l.remove(1));
		assertEquals("[3.0 3.0 4.0 5.0 4.0]", l.toString());
		assertTrue(l.remove(4));
		assertEquals("[3.0 3.0 5.0 4.0]", l.toString());
		assertFalse(l.remove(25));
		assertEquals("[3.0 3.0 5.0 4.0]", l.toString());
		assertTrue(l.removeFirst());
		assertEquals("[3.0 5.0 4.0]", l.toString());
		assertTrue(l.removeLast());
		assertEquals("[3.0 5.0]", l.toString());
		l = new LinkedList();
		assertFalse(l.removeFirst());
		assertFalse(l.removeLast());
		assertEquals("[]", l.toString());
	}
	
	/**
	 * Test metodo mean:
	 * Il metodo mean deve esistere. Calcola la media aritmetica
	 * degli elementi presenti nella lista.
	 */
	@Test
	public void test7() {
		try{
			LinkedList.class.getMethod("mean");
		}catch(NoSuchMethodException e){
			fail("Method not yet implemented");
		}
		LinkedList l = new LinkedList("1");
		assertEquals(1, l.mean(), 0.01);
		l.remove(1);
		assertEquals(0, l.mean(), 0.01);
		l = new LinkedList("160 591 114 229 230 270 128 1657 624 1503");
		assertEquals(550.6, l.mean(), 0.01);
		l = new LinkedList("15.0 69.9 6.5 22.4 28.4 65.9 19.4 198.7 38.8 138.2");
		assertEquals(60.32, l.mean(), 0.01);
	}
	
	/**
	 * Test metodo stdDev:
	 * Il metodo stdDev deve esistere. Calcola la deviazione standard
	 * degli elementi presenti nella lista.
	 */
	@Test
	public void test8() {
		try{
			LinkedList.class.getMethod("stdDev");
		}catch(NoSuchMethodException e){
			fail("Method not yet implemented");
		}
		LinkedList l = new LinkedList("");
		assertEquals(0, l.stdDev(), 0.01);
		l.addLast(1);
		assertEquals(0, l.stdDev(), 0.001);
		l = new LinkedList("160 591 114 229 230 270 128 1657 624 1503");
		assertEquals(572.03, l.stdDev(), 0.01);
		l = new LinkedList("15.0 69.9 6.5 22.4 28.4 65.9 19.4 198.7 38.8 138.2");
		assertEquals(62.26, l.stdDev(), 0.01);
	}
	
	/**
	 * Test metodi next e prev:
	 * I metodi next e prev devono esistere. L'utilita' di questi due metodi e'
	 * quella di scorrere tra gli elementi della lista avanti e indietro.
	 * Il metodo next ritorna l'elemento corrente e posiziona il cursore all'elemento successivo.
	 * Se siamo posizionati sull'ultimo elemento, il cursore non si sposta.
	 * Il metodo prev ritorna l'elemento corrente e posiziona il cursore all'elemento precedente.
	 * Se siamo posizionati sul primo elemento, il corsore non si sposta.
	 */
	@Test
	public void test9() {
		try{
			LinkedList.class.getMethod("next");
			LinkedList.class.getMethod("prev");
		}catch(NoSuchMethodException e){
			fail("Method not yet implemented");
		}
		LinkedList l = new LinkedList("1 2 3");
		assertEquals(1, l.next(), 0.001);
		assertEquals(2, l.next(), 0.001);
		assertEquals(3, l.next(), 0.001);
		assertEquals(3, l.next(), 0.001);
		assertEquals(3, l.prev(), 0.001);
		assertEquals(2, l.prev(), 0.001);
		assertEquals(1, l.prev(), 0.001);
		assertEquals(1, l.prev(), 0.001);
	}
	
	/**
	 * Test costruttore da file:
	 * Deve esistere un cotruttore che accetta come parametro un oggetto di tipo FileReader.
	 * Il costruttore legge gli elementi dal file specificato (input.txt) e inizializza la lista.
	 */
	@Test
	public void test10() {
		try{
			LinkedList.class.getConstructor(java.io.FileReader.class);
		}catch(NoSuchMethodException e){
			fail("Constructor not yet implemented");
		}
		File f = new File("src/input.txt");
		try {
			LinkedList l = new LinkedList(new java.io.FileReader(f));
			assertEquals("[7.5 10.3 8.95 6.72 7.0 4.35 10.02]", l.toString());
			assertEquals(7.83, l.mean(), 0.01);
			assertEquals(2.09, l.stdDev(), 0.01);
		} catch (FileNotFoundException e) {
			fail("File does not exist");
		}
		
	}

}
