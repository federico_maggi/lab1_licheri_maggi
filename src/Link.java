
public class Link {

    public double data;
    public Link nextLink;
    public Link preLink;

    //Costruttore
    public Link( double d1) {
	    data = d1;
    }

    //toString
    public String toString() {
	    return "" + this.data;
    }
}