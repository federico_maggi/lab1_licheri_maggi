import java.util.Scanner;
import java.io.FileReader;


public class LinkedList {
	private Link first;
	private Link last;
	private int count_elements;
	private Link current;

    //Costruttore
    public LinkedList() {
	    first = null;
	    last=null;
	    count_elements = 0;
    }
    
    //Costruttore con input stringa
    public LinkedList(String s){
    	this();
	    if(s!=""){
	    	String[] stringaSplit = s.split(" ");
	    	for(int i = 0; i<stringaSplit.length; i++){
	    		this.addLast(Double.parseDouble(stringaSplit[i]));
	    	}
    	}    	
    }

    //Ritorna true se la lista è vuota
    public boolean isEmpty() {
	    return (first == null && last==null);
    }

    //Prints list data
    public String toString() {
    	String str;
	    Link currentLink = first;
	    str = "[";
	    for(int i = 0; i<count_elements; i++){
		    str = str + currentLink.toString() + " ";
		    currentLink = currentLink.nextLink;
	    }
	    String sub;
	    if(count_elements == 0){
	    	sub=str;
	    }else{
	    	sub = str.substring(0, str.length() - 1);
	    }
	    return sub + "]";
    }
    
    //aggiunge elemento in cima alla lista
    public void addFirst(double d){
	    Link link = new Link(d);
	    if(count_elements != 0){
	    	link.nextLink = first;
	    }else{
	    	last=link;
	    }
	    first = link;
	    first.preLink = null;
	    count_elements = count_elements + 1;
    }
    
    //aggiunge elemento in fondo alla lista
    public void addLast(double d){
	    Link link = new Link(d);
	    link.preLink = last;
	    if(count_elements==0){
	    	first = link;
	    }else{
	    	last.nextLink =link;
	    }
	    last = link;
	    last.nextLink=null;
	    count_elements = count_elements + 1;
    }
    
    //se le stringhe delle due liste sono uguali allora le due liste sono uguali
    public boolean equals(Object o){
    	if(this.toString().equals(o.toString())){
    		return true;
    	}else{
    	return false;
    	}
    }
    
    //rimuove la prima occorrenza del numero passato in input
    public boolean remove(double d){
    	Link list = this.first;
    	Link list2;
    	Link list3;
    	for(int i = 0; i<count_elements; i++){
    		if(list.data==d){

    			if(i==0){
    				this.removeFirst();
        			System.out.println("primo" + this.toString());
    			}else if(i==count_elements - 1){
    				this.removeLast();
        			System.out.println("ultimo" + this.toString());
    			}else{
    				list2 = list.preLink;
    				list3 = list.nextLink;
    				list2.nextLink = list3;
    				list3.preLink = list2;
    				count_elements = count_elements - 1;
    			}
    			return true;
    		}
    		list = list.nextLink;
    	}
    	return false;
    }
    
    //rimuove il primo elemento della lista
    public boolean removeFirst(){
    	if(count_elements>0){
    		if(count_elements==1){
    			first = null;
    			last = null;
    		}else{
    			first = first.nextLink;
    			first.preLink = null;
    		}
    		count_elements = count_elements -1;
    		return true;
    	}else{
    		return false;
    	}
    }    
    
    //rimuove l'ultimo elemento della lista
    public boolean removeLast(){
    	if(count_elements>0){
    		if(count_elements==1){
    			first = null;
    			last = null;
    		}else{
    			last = last.preLink;
    			last.nextLink = null;
    		}
    		count_elements = count_elements -1;
    		return true;
    	}else{
    		return false;
    	}
    }
    
    //calcola la media
    public double mean(){
    	Link l = first;
    	double media=0;
    	for(int i=0; i<count_elements; i++){
    		media = media + l.data;
    		l = l.nextLink;
    	}
    	if(count_elements != 0){
    		return media / count_elements;
    	}else{
    		return media;
    	}
    }
    
    //calcola  la deviazione standard
    public double stdDev(){
    		Link l = first;
    		double x=0;
        	if(count_elements!=1){    		
    		for(int i=0; i<count_elements; i++){
    			x= x + Math.pow(l.data - this.mean(),2);
    			l = l.nextLink;
    		}
    		x = Math.sqrt(x / (count_elements - 1));
    	}
    	return x;
    }
    
    //restituisce l'elemento puntato e sposta il puntatore al successivo;
    public double next(){
    	if(current==null){
    		current=first;
    	}
    	if(current.nextLink!=null){
    		current=current.nextLink;
    		return (current.preLink).data;
    	}else{
    		return current.data;
    	}
    }
    
    //restituisce l'elemento puntato e sposta il puntatore al precedente;
    public double prev(){
    	if(current==null){
    		current=last;
    	}
    	if(current.preLink!=null){
    		current=current.preLink;
    		return (current.nextLink).data;
    	}else{
    		return current.data;
    	}

    }
    
    //Crea una lista prendendo in input un file
    public LinkedList(FileReader f){
    	this();
    	Scanner scn = new Scanner(f).useDelimiter(" ");
    	while(scn.hasNext()){
    		double token = Double.parseDouble(scn.next());
    		this.addLast(token);
    	}
    }
    
}
